export interface CityInterface {
  city: string;
  todayMax: number;
  todayMin: number;
  todayIcon: string;
  tomorrowMax: number;
  tomorrowMin: number;
  tomorrowIcon: string;
  thirdMax: number;
  thirdMin: number;
  thirdIcon: string
}
