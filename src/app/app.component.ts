import { Component, OnInit } from '@angular/core';
import { MatTabsModule } from '@angular/material/tabs';
import { ForecastService } from './services/forecast.service';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { CdkTableModule } from '@angular/cdk/table';
import { CdkTreeModule } from '@angular/cdk/tree';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {
  title = 'app';
  cities = [];
  selectedTab: number;
  foundCities = [];
  foundCity = {
  }

  constructor(public forecastService: ForecastService) { }

  ngOnInit() {

  }

  /**
  * Method to receive and show the cities
  **/
  checkCities(citiesToShow) {
    console.log(citiesToShow);
    this.foundCities = citiesToShow;
    const length = this.foundCities.length;
    this.selectedTab = length - 1;
  }

  /**
  * Method to add the city to the found cities array
  **/
  putCityData(city) {
    this.foundCities.push(city);
  }

  /**
  * Method to remove cities from the X on the tab
  **/
  removeTab(index) {
    this.foundCities.splice(index, 1)
  }

}
