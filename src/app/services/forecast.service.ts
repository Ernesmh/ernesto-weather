import { Injectable } from '@angular/core';
import { Observable, Subject, pipe } from 'rxjs';
import { map } from 'rxjs/operators';
import { HttpClient } from '@angular/common/http';
import { environment } from '../../environments/environment';


const BASEURL = environment.URL + "/data/2.5/forecast?q=";
const ENVIRONMENT = environment.APIKEY;
const unit = '&units=metric'

@Injectable()
export class ForecastService {
  modifiedResponse = {
  city: ''
 };

  constructor(private http: HttpClient) { }

  /**
  * Method to make the api request and get the weather
  **/
  getCityWeather(cityName): Observable<any> {
    const f = BASEURL + cityName + unit + ENVIRONMENT;

    return this.http.get(BASEURL + cityName + unit + ENVIRONMENT)
          // .pipe(map((res) =>  ));

  }
}
