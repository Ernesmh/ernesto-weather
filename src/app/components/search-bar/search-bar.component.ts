import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { NgxLoadingModule } from 'ngx-loading';
import { ForecastService } from '../../services/forecast.service';
import { CityInterface } from '../../interfaces/cities';

@Component({
  selector: 'app-search-bar',
  templateUrl: './search-bar.component.html',
  styleUrls: ['./search-bar.component.css']
})
export class SearchBarComponent implements OnInit {

  @Output()
  citiesEmitter: EventEmitter<Array<string>> = new EventEmitter<Array<string>>();
  @Output()
  foundCityEmitter: EventEmitter<object> = new EventEmitter<object>();

  city = '';
  cities = [];
  foundCities = [];
  foundCity: CityInterface;
  repeated = false;
  errorText: string;
  repeatedText: string;
  repeatedName: string;
  loading = false;

  constructor(private forecastService: ForecastService) { }

  ngOnInit() {
  }

  /**
  * Method to show the selected city
  **/
  showCity() {
    this.city = this.city[0].toUpperCase() + this.city.substr(1).toLowerCase();
    if (this.foundCities) {
      for (let i = 0; i < this.foundCities.length; i++) {
        if (this.foundCities[i].city === this.city)
          this.repeated = true;
        this.repeatedName = this.city;
      }
    }
    if (this.repeated === true) {
      this.repeatedText = this.repeatedName + ' has already been chosen';
      this.repeated = false;
      this.errorText = '';
      this.repeatedName = '';
    } else {
      this.city = this.city.toLowerCase();
      this.loading = true;
      this.forecastService.getCityWeather(this.city)
        .subscribe(res => {
          this.loading = false;
          const newFoundCity = {
            city: res.city.name,
            todayMax: res.list[0].main.temp_max.toFixed(1),
            todayMin: res.list[0].main.temp_min.toFixed(1),
            todayIcon: 'http://openweathermap.org/img/w/' + res.list[0].weather[0].icon + '.png',
            tomorrowMax: res.list[8].main.temp_max.toFixed(1),
            tomorrowMin: res.list[8].main.temp_min.toFixed(1),
            tomorrowIcon: 'http://openweathermap.org/img/w/' + res.list[8].weather[0].icon + '.png',
            thirdMax: res.list[16].main.temp_min.toFixed(1),
            thirdMin: res.list[16].main.temp_min.toFixed(1),
            thirdIcon: 'http://openweathermap.org/img/w/' + res.list[15].weather[0].icon + '.png',
          };
          this.foundCities.push(newFoundCity);
          this.citiesEmitter.emit(this.foundCities);
          this.errorText = '';
          this.repeatedText = '';
          this.repeatedName = '';
        },
        err => {
          this.errorText = 'That city could not be found';
          this.loading = false;
        })
      this.city = this.city[0].toUpperCase() + this.city.substr(1).toLowerCase();
    }
  }

  /**
  * Method to remove the city from the list
  **/
  removeCity(index) {
    this.foundCities.splice(index, 1);
    this.citiesEmitter.emit(this.foundCities);
  }
}
